# Change log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.3.2] - 2020-09-23
### Fixed
- Comments from roles were removed if not present in group vars.
- Removing `inventory`, `group_vars` or `roles` from options resulted in a failure.

## [0.3.1] - 2020-06-09
### Fixed
- It was not possible to parse multidocument YAML files.

## [0.3.0] - 2020-05-02
### Added
- Increased output during task run.
- Possiblity to have mutliple targets.
- The default value for the variables is cast to a YAML string.

## [0.2.0] - 2020-04-30
### Added
- Values for groups and variables are saved.

### Changed
- Variables and inventory groups are returned as objects instead of strings. The previous value is at the `description` key in the object.

### Fixed
- The plugin would crash if the inventory didn&#39;t exist.

## [0.1.1] - 2018-09-11
### Fixed
- If a var file without comments was parsed it would overwrite comments from previous files.
- The URL in the changelog for comparing versions was incorrect.

## [0.1.0] - 2018-09-11
### Added
- Parse comments from inventory files.
- Parse comments in group_vars.
- Parse comments of roles&#39; default vars.

[0.3.2]: https://gitlab.com/ephracis/grunt-ansible-docs/compare/0.3.1...0.3.2
[0.3.1]: https://gitlab.com/ephracis/grunt-ansible-docs/compare/0.3.0...0.3.1
[0.3.0]: https://gitlab.com/ephracis/grunt-ansible-docs/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/ephracis/grunt-ansible-docs/compare/0.1.1...0.2.0
[0.1.1]: https://gitlab.com/ephracis/grunt-ansible-docs/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/ephracis/grunt-ansible-docs/compare/704d30f...0.1.0
