# grunt-ansible-docs

A grunt plugin for generating documentation for Ansible from inline comments.
It generates documentation for inventory groups, and variables read from both
roles and group_vars.

## Example

Given an inventory file `inventory`:

```ini
# The primary server
[primary]
server1

# The replica server
[replica]
server2
```

...a role with a default vars file at `roles/myrole/defaults/main.yml`:

```yaml
# A role variable
role_var: foo

# Another role variable
second_role_var: bar
```

..and a group vars file `group_vars/all.yml`:

```yaml
# A playbook variable
play_var: one

# An override of the role var
role_var: two
```

...running the following:

```shell
grunt ansible-docs
```

...would produce a JSON file at `ansible_docs.json`:

```json
{
  "vars": {
    "play_var": { "description": "A playbook variable", "value": "one" },
    "role_var": { "description": "An override of the role var", "value": "two" },
    "second_role_var": { "description": "Another role variable", "value": "bar" }
  },
  "inventory": {
    "primary": { "description": "The primary server", "value": "server1" },
    "replica": { "description": "The replica server", "value": "server2" }
  }
}
```

This JSON file can then be further processed to generate a Markdown file,
LaTeX document, HTML website, or whatever.

## Getting Started

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out
the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains
how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as
install and use Grunt plugins. Once you're familiar with that process, you may
install this plugin with this command:

```shell
npm install grunt-ansible-docs --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with
this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-ansible-docs');
```

## Ansible docs task

_Run this task with the `grunt ansible-docs` command._

Task targets, files and options may be specified according to the grunt
[Configuring tasks](http://gruntjs.com/configuring-tasks) guide.

### Options

#### inventory

Type: `String`

Path to a inventory file in INI format.

#### roles

Type: `String`

Path to the roles folder.

#### group_vars

Type: `String`

Path to the group_vars folder.

#### dest

Type: `String`

Path to the JSON file where the documentation will be written.

## Example

Example configuration with multiple targets:

```js
"ansible-docs": {
  options: {
    inventory: 'path/to/inventory'
    roles: 'path/to/roles'
    group_vars: 'path/to/group_vars'
    dest: 'output/docs.json'
  },
  target_1: {
    inventory: 'another/inventory'
    dest: 'output/docs_1.json'
  },
  target_2: {
    inventory: 'target_2/inventory'
    roles: 'target_2/roles'
    group_vars: 'target_2/group_vars'
    dest: 'output/docs_2.json'
  }
}
```
